package com.example.funfacts;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.content.Context;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static android.provider.Settings.System.getString;
import static androidx.core.content.ContextCompat.getSystemService;

/**
 * Implementation of App Widget functionality.
 */
public class FactWidget extends AppWidgetProvider {
    public static AppWidgetManager widgetManager;
    public static int widgetId;
    public static String currFact;
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        widgetManager = appWidgetManager;
        widgetId = appWidgetId;

        getNextFact(context, new VolleyCallBack() {
            @Override
            public void onSuccess() {
                // Construct the RemoteViews object
                RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.fact_widget);
                Log.d("res", currFact);
                views.setTextViewText(R.id.appwidget_text, currFact);
                // Instruct the widget manager to update the widget
                widgetManager.updateAppWidget(widgetId, views);

            }
        });

        RemoteViews remoteV = new RemoteViews(context.getPackageName(), R.layout.fact_widget);

        Intent intentSync = new Intent(context, FactWidget.class);
        intentSync.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        PendingIntent pendingSync = PendingIntent.getBroadcast(context,0, intentSync, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteV.setOnClickPendingIntent(R.id.button_refresh, pendingSync);

        widgetManager.updateAppWidget(appWidgetId, remoteV);

    }


    public static void getNextFact(Context context, VolleyCallBack callBack) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
//        String url ="https://uselessfacts.jsph.pl/random.json?language=en";
        String url ="https://useless-facts.sameerkumar.website/api";
        // Request a string response from the provided URL.
        Log.d("req", "sending request");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        JSONObject res = new JSONObject();
                        try {
                            res = new JSONObject(response);
//                            widgetText[0] = res.getString("text");
                            currFact = res.getString("data");
                            callBack.onSuccess();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                currFact = "That didn't work!";

            }
        });
        queue.add(stringRequest);
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.d("receive", "In receive");
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName thisAppWidget = new ComponentName(context.getPackageName(), FactWidget.class.getName());
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
        updateAppWidget(context, appWidgetManager, appWidgetIds[0]);
    }

    public interface VolleyCallBack {
        void onSuccess();
    }
}