package com.example.funfacts;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

class NotificationHelper {

    private Context mContext;
    private String fact;
    private static final String NOTIFICATION_CHANNEL_ID = "10001";

    NotificationHelper(Context context) {
        mContext = context;
    }

    void createNotification()
    {

        Intent intent = new Intent(mContext , MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        RequestQueue queue = Volley.newRequestQueue(mContext);
//        String url ="https://uselessfacts.jsph.pl/random.json?language=en";
        String url ="https://useless-facts.sameerkumar.website/api";
        // Request a string response from the provided URL.
        Log.d("req", "sending request");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        JSONObject res = new JSONObject();
                        try {
                            res = new JSONObject(response);
                            fact = res.getString("data");
                            // Construct the RemoteViews object
                            RemoteViews views = new RemoteViews(mContext.getPackageName(), R.layout.fact_widget);
                            views.setTextViewText(R.id.appwidget_text, fact);
                            // Instruct the widget manager to update the widget
                            FactWidget.widgetManager.updateAppWidget(FactWidget.widgetId, views);
                            FactWidget.currFact = fact;
                            // Display Notification
                            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, NOTIFICATION_CHANNEL_ID);
                            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                            mBuilder.setContentTitle("Fact of the Day!")
                                    .setContentText(fact)
                                    .setAutoCancel(false)
                                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                                    .setContentIntent(resultPendingIntent);

                            NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
                            {
                                int importance = NotificationManager.IMPORTANCE_HIGH;
                                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                                notificationChannel.enableLights(true);
                                notificationChannel.setLightColor(Color.RED);
                                notificationChannel.enableVibration(true);
                                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                                assert mNotificationManager != null;
                                mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                                mNotificationManager.createNotificationChannel(notificationChannel);
                            }
                            assert mNotificationManager != null;
                            mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                fact = "That didn't work!";

            }
        });
        queue.add(stringRequest);
    }
}